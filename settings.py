__author__ = 'Ai Jogja'

import json
from PyQt4 import QtCore, QtGui
# Import UI
from harvest_ui.settings_dialog_ui import Ui_SettingsDialog
from random import randint

class BaseSettings(object):

    def get_json_file(self):
        try:
            with open('settings.json', 'r') as f:
                config = json.load(f)
            return config
        except IOError:
            self.set_json_file()
            return self.get_json_file()
    
    def set_json_file(self, data={}):
        with open('settings.json', 'w') as f:
            json.dump(data, f) 

"""
Settings Dialog
"""
class Settings(BaseSettings, QtGui.QDialog):

    def __init__(self):
        QtGui.QWidget.__init__(self)
        self.ui = Ui_SettingsDialog()
        self.ui.setupUi(self)
        self.interval = 1
        self.setting_data = self.get_json_file()
        self.interval_settings = self.setting_data.get('interval', 1)
        self.__set_default_value()
        self.__button_connect()
        self.ui.ActionBox.button(QtGui.QDialogButtonBox.Save).clicked.connect(self.setting_action)

    def get_interval_value(self):
        interval = self.interval_settings
        if isinstance(interval, list):
            interval = (randint(interval[0],interval[1]))
        return interval

    def __button_connect(self):
        self.ui.radioInterval1.clicked.connect(self.__check_interval)
        self.ui.radioInterval2.clicked.connect(self.__check_interval)
        self.ui.radioInterval3.clicked.connect(self.__check_interval)
        self.ui.radioInterval4.clicked.connect(self.__check_interval)
        self.ui.radioInterval5.clicked.connect(self.__check_interval)
        self.ui.radioInterval6.clicked.connect(self.__check_interval)
        self.ui.radioInterval7.clicked.connect(self.__check_interval)
        self.ui.radioInterval8.clicked.connect(self.__check_interval)
        self.ui.dial1.valueChanged.connect(self.__get_min_value)
        self.ui.dial2.valueChanged.connect(self.__get_max_value)

    def __set_default_value(self):
        interval = self.interval_settings
        if isinstance(interval, list):
            self.ui.radioInterval8.setChecked(True)
            self.__set_default_random_value(interval)
        elif interval == 300:
            self.ui.radioInterval7.setChecked(True)
        elif interval == 120:
            self.ui.radioInterval6.setChecked(True)
        elif interval == 60:
            self.ui.radioInterval5.setChecked(True)
        elif interval == 20:
            self.ui.radioInterval4.setChecked(True)
        elif interval == 10:
            self.ui.radioInterval3.setChecked(True)
        elif interval == 5:
            self.ui.radioInterval2.setChecked(True)
        else:
            self.ui.radioInterval1.setChecked(True)
        self.__check_interval()

    def __set_default_random_value(self,interval):
        self.ui.dial1.setProperty("value",interval[0])
        self.ui.dial2.setProperty("value",interval[1])
        self.__get_min_value()
        self.__get_max_value()

    def __check_interval(self):
        if self.ui.radioInterval8.isChecked():
            self.ui.groupBox.setEnabled(True)
            self.interval = 0
        else:
            self.ui.groupBox.setEnabled(False)
            if self.ui.radioInterval1.isChecked():
                self.interval = 1
            elif self.ui.radioInterval2.isChecked():
                self.interval = 5
            elif self.ui.radioInterval3.isChecked():
                self.interval = 10
            elif self.ui.radioInterval4.isChecked():
                self.interval = 20
            elif self.ui.radioInterval5.isChecked():
                self.interval = 60
            elif self.ui.radioInterval6.isChecked():
                self.interval = 120
            elif self.ui.radioInterval7.isChecked():
                self.interval = 300

    def __get_min_value(self):
        if self.ui.dial1.value() > 1:
            value_text = 'seconds'
        else:
            value_text = 'second'
        self.ui.lblDialMinValue.setText('%d %s'%(self.ui.dial1.value(), value_text))

    def __get_max_value(self):
        if self.ui.dial2.value() > 1:
            value_text = 'seconds'
        else:
            value_text = 'second'
        self.ui.lblDialMaxValue.setText('%d %s'%(self.ui.dial2.value(), value_text))

    def setting_action(self):
        if self.interval == 0:
            self.interval = [self.ui.dial1.value(), self.ui.dial2.value()]

        self.setting_data['interval'] = self.interval
        self.set_json_file(self.setting_data)
        self.accept()

class SelectedDirectory(BaseSettings):

    def __init__(self):
        self.setting_data = self.get_json_file()

    def get_directory(self):
        return self.setting_data.get('save_to_dir', '')

    def set_directory(self, path_dir):
        self.setting_data['save_to_dir'] = str(path_dir)
        self.set_json_file(self.setting_data)
        return path_dir