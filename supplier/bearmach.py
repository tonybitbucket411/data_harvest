__author__ = 'Ai Jogja'

from selenium import webdriver
from settings import Settings

import time

class Bearmach(object):
    def __init__(self):
        self.url = 'http://orders.bearmach-online.co.uk/'
        self.driver = ''
        self.settings = Settings()

    def Login(self, email, password):
        try:
            #   Open Browser and goto url
            # driver = webself.driver.PhantomJS('phantomjs')
            self.driver = webdriver.Firefox()
            self.driver.get(self.url)
            actions = webdriver.ActionChains(self.driver)
            #   Input email and password
            elemen = self.driver.find_element_by_name("identification")
            elemen.send_keys(email)
            elemen = self.driver.find_element_by_name("password")
            elemen.send_keys(password)
            elemen.submit()
            #   Result
            try:
                partcode = self.driver.find_element_by_id("id_part_code")
                if partcode:
                    return True
                else:
                    self.driver.close()
                    return False
            except:
                self.driver.close()
                return False
        except:
            self.driver.close()
            return False

    def __delay(self):
        interval = self.settings.get_interval_value()
        return time.sleep(interval)

    def Navigate(self):
        return True

    def DoSearchPart(self, partnumber):
        try:

            elemen = self.driver.find_element_by_name("part_code")
            elemen.send_keys(partnumber);self.__delay()
            elemen.submit();self.__delay()

            xpath_table = "//table[@id='searchresults']//tr"
            result_table = self.driver.find_elements_by_xpath(xpath_table)

            result_scrap = []
            for idx_tr in range(len(result_table)):

                xpath_tr = xpath_table + "[" + str(idx_tr) + "]//td"
                result_tr = self.driver.find_elements_by_xpath(xpath_tr)

                if len(result_tr) < 5:
                    continue

                result_item = []
                for idx_td in range(0, 7):
                    result_item.append(result_tr[idx_td].text)

                result_scrap.append(result_item)

            return   result_scrap

            # self.driver.back();
            # self.__delay()

        except Exception:
            print "Exception"
            pass

    def CloseDriver(self):
        try:
            self.driver.quit()
            return True
        except:
            pass
