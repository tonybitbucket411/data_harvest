__author__ = 'Tony Adam'

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from settings import Settings

import time

class Hotbray(object):

    def __init__(self):

        self.url = 'http://www.hotsupport.co.uk'
        self.driver = ''
        self.settings = Settings()

    def Login(self, email, password):

        try:
            #   Open Browser and goto url
            # driver = webself.driver.PhantomJS('phantomjs')
            self.driver = webdriver.Firefox()
            self.driver.get(self.url)
            actions = webdriver.ActionChains(self.driver)
            #   Input email and password
            elemen = self.driver.find_element_by_name("tx_UserName")
            elemen.send_keys(email)
            elemen = self.driver.find_element_by_name("tx_Password")
            elemen.send_keys(password)
            self.driver.find_element_by_name('but_OK').click()
            self.__delay()

            return True

        except:

            self.driver.close()
            return False

    def __delay(self):
        interval = self.settings.get_interval_value()
        return time.sleep(interval)

    def Navigate(self):

        self.driver.find_element_by_id('ImageButton2').click()
        self.__delay()

        return True

    def DoSearchPart(self, partnumber):

        try:

            elemen = self.driver.find_element_by_name("tx_PartNumber")
            elemen.clear()
            elemen.send_keys(partnumber)
            self.__delay()
            # elemen.submit();self.__delay()

            self.driver.find_element_by_name("Button1").click()
            self.__delay()

            xpath_table = "//table[@id='Table1']//tr"
            result_table = self.driver.find_elements_by_xpath(xpath_table)

            result_scrap = []
            for idx_tr in range(3, len(result_table)):

                xpath_tr = xpath_table + "[" + str(idx_tr) + "]//td"
                result_tr = self.driver.find_elements_by_xpath(xpath_tr)

                if len(result_tr) < 2:
                    continue

                result_item = []
                for idx_td in range(len(result_tr)):
                    result_item.append(result_tr[idx_td].text)

                result_scrap.append(result_item)

            return result_scrap

        except Exception:

            pass

    def CloseDriver(self):

        try:
            self.driver.quit()
            return True
        except:
            pass

# hotbray = Hotbray()
# hotbray.Login('j7601', 'paper14')
# hotbray.Navigate()
# hotbray.DoSearchPart('1013938')


