__author__ = 'Tony Adam'

from selenium import webdriver
from settings import Settings
import time

class FPS(object):
    def __init__(self):
        self.url = 'https://fdrive.fpsdistribution.co.uk/user/login'
        self.driver = ''
        self.settings = Settings()

    def Login(self, email, password):
        try:
            #   Open Browser and goto url
            self.driver = webdriver.Firefox()

            self.driver.get(self.url)
            self.__delay()
            actions = webdriver.ActionChains(self.driver)

            elemen = self.driver.find_element_by_name("username")
            elemen.send_keys(email); self.__delay()
            elemen = self.driver.find_element_by_name("password")
            elemen.send_keys(password); self.__delay()
            elemen.submit()

            self.__delay()

            return True

        except:

            self.CloseDriver()

            return False

    def __delay(self):
        # interval = 1

        interval = self.settings.get_interval_value()

        return time.sleep(interval)

    def Navigate(self):

        self.driver.find_element_by_link_text("Component Parts").click()

        return True

    def DoSearchPart(self, partnumber):

        try:

            elemen = self.driver.find_element_by_name("part-search")
            elemen.send_keys(partnumber); self.__delay()
            elemen.submit(); self.__delay()

            #   Result search
            xpath_table = "//table[@class='table table-condensed part-list']//tr"
            result_table = self.driver.find_elements_by_xpath(xpath_table)

            result_scrap = []
            for idx_tr in range(0, len(result_table)):

                result_tr = result_table[idx_tr].find_elements_by_xpath('.//td')

                if len(result_tr) < 3:
                    continue

                result_item = []

                result_tr.pop(0)
                result_tr.pop(len(result_tr) - 1)
                result_tr.pop(len(result_tr)-1)

                if result_tr[0].find_elements_by_xpath('.//button'):
                    result_item.append('True')
                else:
                    result_item.append('False')
                result_tr.pop(0)

                for idx_td in range(len(result_tr)):
                    info_td = result_tr[idx_td].text.encode('ascii', 'ignore')

                    if len(info_td) < 1:

                        if result_tr[idx_td].find_elements_by_xpath('.//span[@class="stock stock-yes"]'):
                            info_td = 'Available'
                        else:
                            info_td = 'Unavailable'

                    result_item.append(info_td)

                result_scrap.append(result_item)

            self.driver.back();self.__delay()

            return result_scrap

        except Exception:

            pass

    def CloseDriver(self):
        try:
            self.driver.quit()
            return True
        except:
            pass

