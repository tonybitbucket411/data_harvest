__author__ = 'Ai Jogja'

from selenium import webdriver
from settings import Settings
import time

class DavidManners(object):
    def __init__(self):
        self.url = 'http://www.jagspares.co.uk/Manners/company.asp'
        # self.url = 'http://www.jagspares.co.uk'
        self.driver = ''
        self.settings = Settings()

    def Login(self, email, password):
        try:
            #   Open Browser and goto url
            # driver = webself.driver.PhantomJS('phantomjs')
            self.driver = webdriver.Firefox()

            self.driver.get(self.url)
            self.__delay()
            actions = webdriver.ActionChains(self.driver)
            assert "David Manners" in self.driver.title
            #   Click Your account
            actions.click(self.driver.find_element_by_link_text("Your Account"))
            actions.perform(); self.__delay()
            #   Input email and password
            elemen = self.driver.find_element_by_name("emailaddr")
            elemen.send_keys(email); self.__delay()
            elemen = self.driver.find_element_by_name("password")
            elemen.send_keys(password); self.__delay()
            elemen.submit()
            #   Result
            try:
                disp = self.driver.find_element_by_name("cid")
                cid = disp.get_attribute("value")
                if cid:
                    self.__delay()
                    return True
                else:
                    self.CloseDriver()
                    return False
            except:
                self.CloseDriver()
                return False
        except:
            self.CloseDriver()
            return False

    def __delay(self):
        interval = self.settings.get_interval_value()
        return time.sleep(interval)

    def Navigate(self):
        self.driver.find_element_by_link_text("Part Number").click()
        return True

    def DoSearchPart(self, partnumber):
        # print partnumber

        try:
            result_scrap = []
            #   Click Part Number
            self.driver.find_element_by_link_text("Part Number").click(); self.__delay()
            #   Input partnumber LNA1600AA
            elemen = self.driver.find_element_by_name("partno")
            elemen.send_keys(partnumber); self.__delay()
            elemen.submit(); self.__delay()
            #   Result search
            xpath_table = "//table[2]//tr"
            result_table = self.driver.find_elements_by_xpath(xpath_table)
            if(result_table):
                result_table.pop(0)
                for idx_tr in range(len(result_table)):
                    idx_tr = idx_tr + 2
                    xpath_tr = "//table[2]//tr["+str(idx_tr)+"]//td"
                    result_tr = self.driver.find_elements_by_xpath(xpath_tr)
                    result_tr[5].find_element_by_css_selector('a').click(); self.__delay()
                    #   View clicked
                    xpath_view_table = "//table[1]//tr"
                    result_view_table = self.driver.find_elements_by_xpath(xpath_view_table)
                    #   Loop The TR value
                    result_item = []
                    for idx_view_tr in range(len(result_view_table)):

                        xpath_view_tr = "//table[1]//tr[" + str(idx_view_tr) + "]//td"
                        result_view_tr = self.driver.find_elements_by_xpath(xpath_view_tr)

                        if(len(result_view_tr) > 1 and result_view_tr[1].text):
                            result_item.append(result_view_tr[1].text.encode('ascii', 'ignore'))
                        elif len(result_view_tr) > 1 and ("Quantity" in result_view_tr[0].text):
                            xpath_view_td = xpath_view_tr + "//input"
                            result_view_td = self.driver.find_elements_by_xpath(xpath_view_td)
                            result_item.append(result_view_td[0].get_attribute("value"))

                    searchQuery = "partno=" + result_item[0]
                    result_item.insert(0, searchQuery)
                    result_item.append(time.strftime("%m-%d-%y"))
                    result_item.append(time.strftime("%H:%M:%S"))
                    result_scrap.append(result_item)

                    # print result_item

                    self.driver.back(); self.__delay()


            return result_scrap
        except Exception,e:
            print "exception"
            return e

    def CloseDriver(self):    
        try:
            self.driver.quit()
            return True
        except:
            pass