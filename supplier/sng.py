__author__ = 'Tony'

from selenium import webdriver
from settings import Settings
import time

class SNG(object):

    def __init__(self):

        self.url = 'http://www.sngbarratt.com/Home.aspx?b=UK'

        self.settings = Settings()

    def Login(self, email, password):

        try:

            self.driver = webdriver.Firefox()
            self.driver.get(self.url)
            self.__delay()

            actions = webdriver.ActionChains(self.driver)
            actions.click(self.driver.find_element_by_link_text("My Account"))
            actions.perform()
            self.__delay()

            assert "SNG Barratt" in self.driver.title

            #   Input email and password
            elemen = self.driver.find_element_by_id("BodyContent_PageContent_txtEmail")
            elemen.send_keys(email); self.__delay()
            elemen = self.driver.find_element_by_id("BodyContent_PageContent_txtPassword")
            elemen.send_keys(password); self.__delay()
            self.driver.find_element_by_id('BodyContent_PageContent_btnSignIn').click()
            self.__delay()
            time.sleep(3)

            return True

        except:
            self.CloseDriver()
            return False

    def __delay(self):

        interval = self.settings.get_interval_value()

        # interval = 1
        return time.sleep(interval)

    def Navigate(self):
        # self.driver.find_element_by_link_text("Part Number").click()
        return True

    def DoSearchPart(self, partnumber):

        try:

            result_scrap = []

            elemen = self.driver.find_element_by_id("txtSearchTerms")
            elemen.send_keys(partnumber); self.__delay()
            self.driver.find_element_by_id('btnSearchGo').click()
            self.__delay()

            #   Result search
            xpath_products = "//div[@class='productInfo']"
            info_products = self.driver.find_elements_by_xpath(xpath_products)

            print len(info_products)

            for info_product in info_products:

                try:
                    description = info_product.find_element_by_xpath(".//*[@class='productTitle']").text.encode('ascii', 'ignore')
                except:
                    description = ""

                try:
                    partNumber = info_product.find_element_by_xpath(".//*[@class='productPartNo']").text.encode('ascii', 'ignore')
                    partNumber = partNumber.replace('PRODUCT CODE: ', '"')

                except:
                    partNumber = ""

                # try:
                #     productApplication = info_product.find_element_by_xpath(".//*[@class='productApplication']").text.encode(
                #         'ascii', 'ignore')
                #
                # except:
                #     productApplication = ""

                try:
                    buyPrice = info_product.find_element_by_xpath(".//*[@class='productPriceAmount productPrice_Your']").text.encode('ascii', 'ignore')

                except:
                    buyPrice = ""

                try:
                    availablity = info_product.find_element_by_xpath(".//*[@class='status']").text.encode('ascii', 'ignore')

                except:
                    availablity = ""

                result_item = [description, partNumber, buyPrice, availablity]

                result_scrap.append(result_item)

            print result_scrap

            return result_scrap

        except Exception,e:
            print "exception"
            return e

    def CloseDriver(self):

        try:
            self.driver.quit()
            return True
        except:
            pass
