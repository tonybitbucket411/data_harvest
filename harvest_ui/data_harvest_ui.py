# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'harvest_ui/data_harvest.ui'
#
# Created: Tue Feb  9 13:07:28 2016
#      by: PyQt4 UI code generator 4.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_mainWIndow(object):
    def setupUi(self, mainWIndow):
        mainWIndow.setObjectName(_fromUtf8("mainWIndow"))
        mainWIndow.resize(703, 542)
        mainWIndow.setToolButtonStyle(QtCore.Qt.ToolButtonIconOnly)
        self.centralwidget = QtGui.QWidget(mainWIndow)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.btnSelectCSV = QtGui.QPushButton(self.centralwidget)
        self.btnSelectCSV.setGeometry(QtCore.QRect(20, 10, 151, 41))
        self.btnSelectCSV.setObjectName(_fromUtf8("btnSelectCSV"))

        self.radioSupplier2 = QtGui.QRadioButton(self.centralwidget)
        self.radioSupplier2.setGeometry(QtCore.QRect(220, 40, 151, 21))
        self.radioSupplier2.setObjectName(_fromUtf8("radioSupplier2"))
        self.radioGroup = QtGui.QButtonGroup(mainWIndow)
        self.radioGroup.setObjectName(_fromUtf8("radioGroup"))
        self.radioGroup.addButton(self.radioSupplier2)

        self.radioSupplier3 = QtGui.QRadioButton(self.centralwidget)
        self.radioSupplier3.setEnabled(True)
        self.radioSupplier3.setGeometry(QtCore.QRect(220, 60, 151, 21))
        self.radioSupplier3.setObjectName(_fromUtf8("radioSupplier3"))
        self.radioGroup.addButton(self.radioSupplier3)

        self.radioSupplier7 = QtGui.QRadioButton(self.centralwidget)
        self.radioSupplier7.setEnabled(True)
        self.radioSupplier7.setGeometry(QtCore.QRect(220, 80, 151, 21))
        self.radioSupplier7.setObjectName(_fromUtf8("radioSupplier7"))
        self.radioGroup.addButton(self.radioSupplier7)

        self.boxProgress = QtGui.QGroupBox(self.centralwidget)
        self.boxProgress.setGeometry(QtCore.QRect(10, 120, 681, 261))
        self.boxProgress.setObjectName(_fromUtf8("boxProgress"))
        self.progbarTotal = QtGui.QProgressBar(self.boxProgress)
        self.progbarTotal.setGeometry(QtCore.QRect(20, 220, 641, 31))
        self.progbarTotal.setMaximum(4)
        self.progbarTotal.setProperty("value", 0)
        self.progbarTotal.setTextVisible(True)
        self.progbarTotal.setObjectName(_fromUtf8("progbarTotal"))
        self.txtInformations = QtGui.QTextBrowser(self.boxProgress)
        self.txtInformations.setGeometry(QtCore.QRect(20, 90, 641, 101))
        self.txtInformations.setObjectName(_fromUtf8("txtInformations"))
        self.lblTotal = QtGui.QLabel(self.boxProgress)
        self.lblTotal.setGeometry(QtCore.QRect(580, 20, 81, 16))
        self.lblTotal.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.lblTotal.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.lblTotal.setObjectName(_fromUtf8("lblTotal"))
        self.lblWorkingOn = QtGui.QLabel(self.boxProgress)
        self.lblWorkingOn.setGeometry(QtCore.QRect(20, 38, 83, 16))
        self.lblWorkingOn.setObjectName(_fromUtf8("lblWorkingOn"))
        self.lblTotalProgress = QtGui.QLabel(self.boxProgress)
        self.lblTotalProgress.setGeometry(QtCore.QRect(20, 200, 121, 16))
        self.lblTotalProgress.setObjectName(_fromUtf8("lblTotalProgress"))
        self.lblFailed = QtGui.QLabel(self.boxProgress)
        self.lblFailed.setGeometry(QtCore.QRect(500, 60, 80, 16))
        self.lblFailed.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignTrailing | QtCore.Qt.AlignVCenter)
        self.lblFailed.setObjectName(_fromUtf8("lblFailed"))
        self.lblConfirmed = QtGui.QLabel(self.boxProgress)
        self.lblConfirmed.setGeometry(QtCore.QRect(500, 40, 80, 16))
        self.lblConfirmed.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignTrailing | QtCore.Qt.AlignVCenter)
        self.lblConfirmed.setObjectName(_fromUtf8("lblConfirmed"))
        self.lblTimeSoFar = QtGui.QLabel(self.boxProgress)
        self.lblTimeSoFar.setGeometry(QtCore.QRect(20, 59, 91, 16))
        self.lblTimeSoFar.setObjectName(_fromUtf8("lblTimeSoFar"))
        self.lblWorkingOnValue = QtGui.QLabel(self.boxProgress)
        self.lblWorkingOnValue.setGeometry(QtCore.QRect(110, 38, 131, 16))
        self.lblWorkingOnValue.setObjectName(_fromUtf8("lblWorkingOnValue"))
        self.lblConfirmedValue = QtGui.QLabel(self.boxProgress)
        self.lblConfirmedValue.setGeometry(QtCore.QRect(580, 40, 81, 16))
        self.lblConfirmedValue.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.lblConfirmedValue.setObjectName(_fromUtf8("lblConfirmedValue"))
        self.lblFailedValue = QtGui.QLabel(self.boxProgress)
        self.lblFailedValue.setGeometry(QtCore.QRect(580, 60, 81, 16))
        self.lblFailedValue.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.lblFailedValue.setObjectName(_fromUtf8("lblFailedValue"))
        self.btnStart = QtGui.QPushButton(self.centralwidget)
        self.btnStart.setGeometry(QtCore.QRect(550, 10, 121, 91))
        self.btnStart.setObjectName(_fromUtf8("btnStart"))
        self.txtSaveTo = QtGui.QPlainTextEdit(self.centralwidget)
        self.txtSaveTo.setEnabled(False)
        self.txtSaveTo.setGeometry(QtCore.QRect(130, 420, 561, 30))
        self.txtSaveTo.setReadOnly(True)
        self.txtSaveTo.setPlainText(_fromUtf8(""))
        self.txtSaveTo.setObjectName(_fromUtf8("txtSaveTo"))
        self.lblSaveTo = QtGui.QLabel(self.centralwidget)
        self.lblSaveTo.setGeometry(QtCore.QRect(14, 397, 161, 16))
        self.lblSaveTo.setObjectName(_fromUtf8("lblSaveTo"))
        self.radioSupplier6 = QtGui.QRadioButton(self.centralwidget)
        self.radioSupplier6.setEnabled(False)
        self.radioSupplier6.setGeometry(QtCore.QRect(360, 60, 151, 21))
        self.radioSupplier6.setObjectName(_fromUtf8("radioSupplier6"))
        self.radioGroup.addButton(self.radioSupplier6)
        self.radioSupplier5 = QtGui.QRadioButton(self.centralwidget)
        self.radioSupplier5.setEnabled(False)
        self.radioSupplier5.setGeometry(QtCore.QRect(360, 40, 151, 21))
        self.radioSupplier5.setObjectName(_fromUtf8("radioSupplier5"))
        self.radioGroup.addButton(self.radioSupplier5)
        self.radioSupplier1 = QtGui.QRadioButton(self.centralwidget)
        self.radioSupplier1.setGeometry(QtCore.QRect(220, 20, 151, 21))
        self.radioSupplier1.setObjectName(_fromUtf8("radioSupplier1"))
        self.radioGroup.addButton(self.radioSupplier1)
        self.btnEditSetting = QtGui.QPushButton(self.centralwidget)
        self.btnEditSetting.setGeometry(QtCore.QRect(20, 60, 151, 41))
        self.btnEditSetting.setObjectName(_fromUtf8("btnEditSetting"))
        self.radioSupplier4 = QtGui.QRadioButton(self.centralwidget)
        self.radioSupplier4.setEnabled(True)
        self.radioSupplier4.setGeometry(QtCore.QRect(360, 20, 151, 21))
        self.radioSupplier4.setCheckable(True)
        self.radioSupplier4.setObjectName(_fromUtf8("radioSupplier4"))
        self.radioGroup.addButton(self.radioSupplier4)
        self.btnSaveTo = QtGui.QPushButton(self.centralwidget)
        self.btnSaveTo.setGeometry(QtCore.QRect(5, 416, 121, 42))
        self.btnSaveTo.setObjectName(_fromUtf8("btnSaveTo"))
        self.btnSave = QtGui.QPushButton(self.centralwidget)
        self.btnSave.setGeometry(QtCore.QRect(570, 460, 101, 41))
        self.btnSave.setObjectName(_fromUtf8("btnSave"))
        mainWIndow.setCentralWidget(self.centralwidget)
        self.menubar = QtGui.QMenuBar(mainWIndow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 703, 22))
        self.menubar.setObjectName(_fromUtf8("menubar"))
        mainWIndow.setMenuBar(self.menubar)
        self.statusbar = QtGui.QStatusBar(mainWIndow)
        self.statusbar.setSizeGripEnabled(True)
        self.statusbar.setObjectName(_fromUtf8("statusbar"))
        mainWIndow.setStatusBar(self.statusbar)

        self.retranslateUi(mainWIndow)
        QtCore.QMetaObject.connectSlotsByName(mainWIndow)

    def retranslateUi(self, mainWIndow):
        mainWIndow.setWindowTitle(_translate("mainWIndow", "Myton Automotive - Part Availability & Price Checker (Data Harvester Version 2)", None))
        self.btnSelectCSV.setText(_translate("mainWIndow", "Select CSV to Use", None))

        self.radioSupplier2.setText(_translate("mainWIndow", "David Manners", None))
        self.radioSupplier3.setText(_translate("mainWIndow", "FPS", None))
        self.radioSupplier7.setText(_translate("mainWIndow", "SNG Barratt", None))

        self.boxProgress.setTitle(_translate("mainWIndow", "Progress", None))
        self.txtInformations.setHtml(_translate("mainWIndow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'.SF NS Text\'; font-size:13pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'MS Shell Dlg 2\'; font-size:12pt; font-weight:600;\">Welcome to Data Harvest</span></p></body></html>", None))
        self.lblTotal.setText(_translate("mainWIndow", "0 of 0", None))
        self.lblWorkingOn.setText(_translate("mainWIndow", "Working On:", None))
        self.lblTotalProgress.setText(_translate("mainWIndow", "Total Progress", None))
        self.lblFailed.setText(_translate("mainWIndow", "Failed :", None))
        self.lblConfirmed.setText(_translate("mainWIndow", "Confirmed:", None))
        self.lblTimeSoFar.setText(_translate("mainWIndow", "Time so far:", None))
        self.lblWorkingOnValue.setText(_translate("mainWIndow", "-", None))
        self.lblConfirmedValue.setText(_translate("mainWIndow", "0/0", None))
        self.lblFailedValue.setText(_translate("mainWIndow", "0/0", None))
        self.btnStart.setText(_translate("mainWIndow", "Start", None))
        self.lblSaveTo.setText(_translate("mainWIndow", "Saving information to:", None))
        self.radioSupplier6.setText(_translate("mainWIndow", "Jaguar Unipart", None))
        self.radioSupplier5.setText(_translate("mainWIndow", "Land Rover Unipart", None))
        self.radioSupplier1.setText(_translate("mainWIndow", "Hotbray", None))
        self.btnEditSetting.setText(_translate("mainWIndow", "Edit Scrape Settings", None))
        self.radioSupplier4.setText(_translate("mainWIndow", "Bearmach", None))
        self.btnSaveTo.setText(_translate("mainWIndow", "Save to", None))
        self.btnSave.setText(_translate("mainWIndow", "Save", None))

