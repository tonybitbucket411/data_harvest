# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'harvest_ui/select_column.ui'
#
# Created: Mon Jan 25 05:11:00 2016
#      by: PyQt4 UI code generator 4.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_SelectColumnDialog(object):
    def setupUi(self, SelectColumnDialog):
        SelectColumnDialog.setObjectName(_fromUtf8("SelectColumnDialog"))
        SelectColumnDialog.resize(296, 271)
        SelectColumnDialog.setSizeGripEnabled(False)
        self.buttonBox = QtGui.QDialogButtonBox(SelectColumnDialog)
        self.buttonBox.setEnabled(False)
        self.buttonBox.setGeometry(QtCore.QRect(90, 227, 181, 21))
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName(_fromUtf8("buttonBox"))
        self.listColumnName = QtGui.QListWidget(SelectColumnDialog)
        self.listColumnName.setGeometry(QtCore.QRect(20, 20, 256, 192))
        self.listColumnName.setObjectName(_fromUtf8("listColumnName"))

        self.retranslateUi(SelectColumnDialog)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("accepted()")), SelectColumnDialog.accept)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("rejected()")), SelectColumnDialog.reject)
        QtCore.QMetaObject.connectSlotsByName(SelectColumnDialog)

    def retranslateUi(self, SelectColumnDialog):
        SelectColumnDialog.setWindowTitle(_translate("SelectColumnDialog", "Select a column", None))

