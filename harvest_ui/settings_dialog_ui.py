# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'harvest_ui/settings_dialog.ui'
#
# Created: Wed Nov  4 05:17:20 2015
#      by: PyQt4 UI code generator 4.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_SettingsDialog(object):
    def setupUi(self, SettingsDialog):
        SettingsDialog.setObjectName(_fromUtf8("SettingsDialog"))
        SettingsDialog.resize(451, 294)
        self.ActionBox = QtGui.QDialogButtonBox(SettingsDialog)
        self.ActionBox.setGeometry(QtCore.QRect(20, 250, 401, 21))
        self.ActionBox.setOrientation(QtCore.Qt.Horizontal)
        self.ActionBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Save)
        self.ActionBox.setCenterButtons(False)
        self.ActionBox.setObjectName(_fromUtf8("ActionBox"))
        self.lblLoadInterval = QtGui.QLabel(SettingsDialog)
        self.lblLoadInterval.setGeometry(QtCore.QRect(20, 40, 161, 16))
        self.lblLoadInterval.setObjectName(_fromUtf8("lblLoadInterval"))
        self.lblScrapeSettings = QtGui.QLabel(SettingsDialog)
        self.lblScrapeSettings.setGeometry(QtCore.QRect(20, 20, 151, 16))
        self.lblScrapeSettings.setObjectName(_fromUtf8("lblScrapeSettings"))
        self.radioInterval3 = QtGui.QRadioButton(SettingsDialog)
        self.radioInterval3.setGeometry(QtCore.QRect(122, 70, 91, 21))
        self.radioInterval3.setObjectName(_fromUtf8("radioInterval3"))
        self.radioInterval8 = QtGui.QRadioButton(SettingsDialog)
        self.radioInterval8.setGeometry(QtCore.QRect(331, 90, 82, 21))
        self.radioInterval8.setObjectName(_fromUtf8("radioInterval8"))
        self.radioInterval7 = QtGui.QRadioButton(SettingsDialog)
        self.radioInterval7.setGeometry(QtCore.QRect(331, 70, 82, 21))
        self.radioInterval7.setObjectName(_fromUtf8("radioInterval7"))
        self.radioInterval4 = QtGui.QRadioButton(SettingsDialog)
        self.radioInterval4.setGeometry(QtCore.QRect(122, 90, 91, 21))
        self.radioInterval4.setObjectName(_fromUtf8("radioInterval4"))
        self.radioInterval5 = QtGui.QRadioButton(SettingsDialog)
        self.radioInterval5.setGeometry(QtCore.QRect(230, 70, 82, 21))
        self.radioInterval5.setObjectName(_fromUtf8("radioInterval5"))
        self.radioInterval1 = QtGui.QRadioButton(SettingsDialog)
        self.radioInterval1.setGeometry(QtCore.QRect(22, 70, 82, 21))
        self.radioInterval1.setChecked(True)
        self.radioInterval1.setObjectName(_fromUtf8("radioInterval1"))
        self.radioInterval2 = QtGui.QRadioButton(SettingsDialog)
        self.radioInterval2.setGeometry(QtCore.QRect(22, 90, 82, 21))
        self.radioInterval2.setObjectName(_fromUtf8("radioInterval2"))
        self.radioInterval6 = QtGui.QRadioButton(SettingsDialog)
        self.radioInterval6.setGeometry(QtCore.QRect(230, 90, 82, 21))
        self.radioInterval6.setObjectName(_fromUtf8("radioInterval6"))
        self.groupBox = QtGui.QGroupBox(SettingsDialog)
        self.groupBox.setEnabled(False)
        self.groupBox.setGeometry(QtCore.QRect(20, 120, 211, 121))
        self.groupBox.setFlat(False)
        self.groupBox.setCheckable(False)
        self.groupBox.setChecked(False)
        self.groupBox.setObjectName(_fromUtf8("groupBox"))
        self.dial1 = QtGui.QDial(self.groupBox)
        self.dial1.setGeometry(QtCore.QRect(20, 30, 71, 64))
        self.dial1.setMinimum(1)
        self.dial1.setMaximum(100)
        self.dial1.setObjectName(_fromUtf8("dial1"))
        self.dial2 = QtGui.QDial(self.groupBox)
        self.dial2.setGeometry(QtCore.QRect(110, 30, 71, 64))
        self.dial2.setMinimum(1)
        self.dial2.setMaximum(100)
        self.dial2.setProperty("value", 100)
        self.dial2.setObjectName(_fromUtf8("dial2"))
        self.lblDialMinValue = QtGui.QLabel(self.groupBox)
        self.lblDialMinValue.setGeometry(QtCore.QRect(27, 100, 71, 16))
        self.lblDialMinValue.setObjectName(_fromUtf8("lblDialMinValue"))
        self.lblDialMaxValue = QtGui.QLabel(self.groupBox)
        self.lblDialMaxValue.setGeometry(QtCore.QRect(109, 100, 81, 16))
        self.lblDialMaxValue.setObjectName(_fromUtf8("lblDialMaxValue"))

        self.retranslateUi(SettingsDialog)
        QtCore.QObject.connect(self.ActionBox, QtCore.SIGNAL(_fromUtf8("accepted()")), SettingsDialog.accept)
        QtCore.QObject.connect(self.ActionBox, QtCore.SIGNAL(_fromUtf8("rejected()")), SettingsDialog.reject)
        QtCore.QMetaObject.connectSlotsByName(SettingsDialog)

    def retranslateUi(self, SettingsDialog):
        SettingsDialog.setWindowTitle(_translate("SettingsDialog", "Settings", None))
        self.lblLoadInterval.setText(_translate("SettingsDialog", "Browser Page Load Interval", None))
        self.lblScrapeSettings.setText(_translate("SettingsDialog", "Scrape Settings", None))
        self.radioInterval3.setText(_translate("SettingsDialog", "10 seconds", None))
        self.radioInterval8.setText(_translate("SettingsDialog", "Random", None))
        self.radioInterval7.setText(_translate("SettingsDialog", "5 minutes", None))
        self.radioInterval4.setText(_translate("SettingsDialog", "20 seconds", None))
        self.radioInterval5.setText(_translate("SettingsDialog", "1 minute", None))
        self.radioInterval1.setText(_translate("SettingsDialog", "1 second", None))
        self.radioInterval2.setText(_translate("SettingsDialog", "5 seconds", None))
        self.radioInterval6.setText(_translate("SettingsDialog", "2 minutes", None))
        self.groupBox.setTitle(_translate("SettingsDialog", "Random Time Interval Selection", None))
        self.lblDialMinValue.setText(_translate("SettingsDialog", "1 second", None))
        self.lblDialMaxValue.setText(_translate("SettingsDialog", "100 seconds", None))

