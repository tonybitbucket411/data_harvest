# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'harvest_ui/login_dialog.ui'
#
# Created: Mon Jan 25 05:10:02 2016
#      by: PyQt4 UI code generator 4.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_LoginDialog(object):
    def setupUi(self, LoginDialog):
        LoginDialog.setObjectName(_fromUtf8("LoginDialog"))
        LoginDialog.resize(283, 174)
        self.Email = QtGui.QLabel(LoginDialog)
        self.Email.setGeometry(QtCore.QRect(40, 40, 51, 16))
        self.Email.setObjectName(_fromUtf8("Email"))
        self.lblPassword = QtGui.QLabel(LoginDialog)
        self.lblPassword.setGeometry(QtCore.QRect(40, 80, 61, 16))
        self.lblPassword.setObjectName(_fromUtf8("lblPassword"))
        self.txtEmail = QtGui.QLineEdit(LoginDialog)
        self.txtEmail.setGeometry(QtCore.QRect(130, 40, 113, 20))
        self.txtEmail.setObjectName(_fromUtf8("txtEmail"))

        self.txtPassword = QtGui.QLineEdit(LoginDialog)
        self.txtPassword.setGeometry(QtCore.QRect(130, 80, 113, 20))
        self.txtPassword.setEchoMode(QtGui.QLineEdit.Password)
        self.txtPassword.setObjectName(_fromUtf8("txtPassword"))

        self.btnLogin = QtGui.QPushButton(LoginDialog)
        self.btnLogin.setGeometry(QtCore.QRect(84, 120, 81, 21))
        self.btnLogin.setObjectName(_fromUtf8("btnLogin"))
        self.btnCancel = QtGui.QPushButton(LoginDialog)
        self.btnCancel.setGeometry(QtCore.QRect(174, 120, 71, 21))
        self.btnCancel.setObjectName(_fromUtf8("btnCancel"))

        self.retranslateUi(LoginDialog)
        QtCore.QMetaObject.connectSlotsByName(LoginDialog)

    def retranslateUi(self, LoginDialog):
        LoginDialog.setWindowTitle(_translate("LoginDialog", "Login", None))
        self.Email.setText(_translate("LoginDialog", "Email", None))
        self.lblPassword.setText(_translate("LoginDialog", "Password", None))
        self.btnLogin.setText(_translate("LoginDialog", "Login", None))
        self.btnCancel.setText(_translate("LoginDialog", "Cancel", None))

