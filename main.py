__author__ = 'Ai Jogja'

import sys
import csv
import urllib2
import json
import os
from datetime import date
from selenium import webdriver
from PyQt4 import QtCore, QtGui
from ConfigParser import SafeConfigParser
import pandas
import numpy
# Import UI
from harvest_ui.data_harvest_ui import Ui_mainWIndow
from harvest_ui.select_column_ui import Ui_SelectColumnDialog
# Import supplier
from supplier.david_manners import DavidManners
from supplier.bearmach import Bearmach
from supplier.hotbray import Hotbray
from supplier.fps import FPS
from supplier.sng import SNG

from login import Login
from settings import Settings, SelectedDirectory


"""
Select Column Dialog
"""
class SelectColumn(QtGui.QDialog):

    def __init__(self, list_columns):
        QtGui.QWidget.__init__(self)
        self.ui = Ui_SelectColumnDialog()
        self.ui.setupUi(self)
        self.row = 0
        self.column = ''
        for column in list_columns:
            item = QtGui.QListWidgetItem()
            item.setText(column)
            self.ui.listColumnName.insertItem(self.row, item)
            self.row += 1

        self.ui.listColumnName.itemClicked.connect(self.item_click)
        self.ui.buttonBox.button(QtGui.QDialogButtonBox.Ok).clicked.connect(self.ok_action)

    def item_click(self, item):
        self.column = item.text()
        if self.column:
            self.ui.buttonBox.setEnabled(True)

    def ok_action(self):
        return self.column

"""
Main Program
"""
class Main(QtGui.QMainWindow):
    def __init__(self):
        QtGui.QMainWindow.__init__(self)
        self.result_data = []
        self.failed_code = []
        self.supplier_name = ''
        self.__new_state()

        self.settings = Settings()
        self.path_dir = SelectedDirectory()

        self.ui = Ui_mainWIndow()
        self.ui.setupUi(self)
        self.ui.btnSelectCSV.clicked.connect(self.select_csv)
        self.ui.btnEditSetting.clicked.connect(self.edit_settings)
        self.ui.btnSaveTo.clicked.connect(self.save_to)
        self.ui.btnStart.clicked.connect(self.do_start_or_stop)
        self.ui.btnSave.clicked.connect(self.save_action)
        self._check_saved_state()
        self._start_stop_button()
        self._save_to_btn()

    def do_start_or_stop(self):
        if not self.is_login:
            if self.is_resume:
                self.do_resume()
            else:
                self.do_start()
        else:
            self.do_stop()

    def do_start(self):
        """
        Action Start
        """
        self.__disable_button()
        self.__reset_ui_display()
        if self.__validation():
            self.do_login()

    def do_resume(self):
        """
        Action Resume
        """
        self.__disable_button()
        self.__resume_ui_display()
        if self.__validation():
            self.do_login()

    def do_stop(self):
        """
        Action Stop
        """
        for thread in self.threads:
            thread.terminate()

        self.set_login_false()
        self.supplier.CloseDriver()
        self._create_saved_state()
        self._check_saved_state()
        self._start_stop_button()

    def set_login_false(self):

        self.is_login = False
        self.__enable_button()
        self._start_stop_button()

    def __new_state(self):
        self.count_success = 0
        self.count_failed = 0
        self.is_login = False
        self.is_resume = False
        self.supplier = ''
        self.partnumber = ''
        self.threads = []
        self.list_code = []
        self.list_code_to_search = []
        self.count_code = len(self.list_code)

    def __validation(self):
        """
        To validate selected CSV file and choseen supplier
        """
        self.get_supplier_selected()

        if not self.list_code:

            self.__enable_button()
            QtGui.QMessageBox.warning(self, "Alert", "Please select CSV file.")

            return False

        elif not self.supplier:

            self.__enable_button()
            QtGui.QMessageBox.warning(self, "Alert", "Please choose a supplier.")

            return False

        else:

            return True

    def __disable_button(self):
        """
        To disable button when scrap is running
        """
        self.ui.btnSelectCSV.setEnabled(False)
        self.ui.btnEditSetting.setEnabled(False)
        self.ui.radioSupplier2.setEnabled(False)
        self.ui.radioSupplier4.setEnabled(False)
        self.ui.btnSave.setEnabled(False)
        self.ui.btnSaveTo.setEnabled(False)

    def __enable_button(self):
        """
        To enable button when scraping stopped
        """
        self.ui.btnSelectCSV.setEnabled(True)
        self.ui.btnEditSetting.setEnabled(True)
        self.ui.radioSupplier2.setEnabled(True)
        self.ui.radioSupplier4.setEnabled(True)
        self.ui.btnSaveTo.setEnabled(True)
        self._save_to_btn()

    def __reset_ui_display(self):

        self.ui.lblWorkingOnValue.setText('-')
        self.ui.lblTotal.setText('0 of 0')
        self.ui.lblConfirmedValue.setText('0/0')
        self.ui.lblFailedValue.setText('0/0')
        self.ui.progbarTotal.setProperty("value",0)

    def __resume_ui_display(self):

        total_scraped = int(self.count_success)+int(self.count_failed)
        self.ui.lblWorkingOnValue.setText(self.partnumber)
        self.ui.lblTotal.setText('%d of %d'%(total_scraped, int(self.count_code)))
        self.ui.lblConfirmedValue.setText('%d/%d'%(int(self.count_success), int(self.count_code)))
        self.ui.lblFailedValue.setText('%d/%d'%(int(self.count_failed), int(self.count_code)))
        self.ui.progbarTotal.setProperty("value",total_scraped)

    def _create_saved_state(self):

        config = SafeConfigParser()
        config.read('.dhs')
        if not config.has_section('main'):
            config.add_section('main')
        config.set('main', 'supplier', str(self.supplier_name))
        config.set('main', 'confirmed', str(self.count_success))
        config.set('main', 'failed', str(self.count_failed))
        config.set('main', 'list_code', str('|'.join(self.list_code)))
        config.set('main', 'partnumber', str(self.partnumber))
        with open('.dhs', 'w') as f:
            config.write(f)

    def _check_saved_state(self):

        config = SafeConfigParser()
        config.read('.dhs')
        # import ipdb; ipdb.set_trace()1```
        if config.sections():
            self.is_resume = True
            self.supplier_name = config.get('main', 'supplier')
            self.count_success = int(config.get('main', 'confirmed'))
            self.count_failed = int(config.get('main', 'failed'))
            self.list_code = config.get('main', 'list_code').split('|')
            self.partnumber = config.get('main', 'partnumber')
            self.count_code = len(self.list_code)
            if self.list_code and self.partnumber:
                self.list_code_to_search = self.list_code[self.list_code.index(self.partnumber):]
            else:
                self.list_code_to_search = self.list_code
            self.set_supplier_selected()

    def _remove_saved_state(self):

        if os.path.isfile('.dhs'):
            os.remove('.dhs')

    def _save_to_btn(self):

        path_dir = self.path_dir.get_directory()
        if path_dir:
            self.ui.txtSaveTo.setPlainText(path_dir)

        if not path_dir or not self.result_data:
            self.ui.btnSave.setEnabled(False)
        else:
            self.ui.btnSave.setEnabled(True)

    def _start_stop_button(self):

        if not self.is_login:
            if self.is_resume:
                self.ui.btnStart.setText("Resume") 
            else:
                self.ui.btnStart.setText("Start") 
        else:
            self.ui.btnStart.setText("Stop")

    def do_login(self):
        """
        Do Login
        """
        if Login(self.supplier).exec_() == QtGui.QDialog.Accepted:

            self.ui.txtInformations.append('<b>Login to website - SUCCESS</b>')
            self.is_login = True

        else:

            self.ui.txtInformations.append('<b>Login to website - FAILED</b>')
            self.set_login_false()

        if self.is_login:
            """
            Do Navigate
            """
            self.do_navigate()
            self._start_stop_button()

    def do_navigate(self):
        """
        Process nagivate thread
        """
        self.navigate_thread = NavigateThread(self.supplier)
        self.navigate_thread.sig.connect(self.__on_navigate_ready)
        self.threads.append(self.navigate_thread)
        self.navigate_thread.start()

    def __on_navigate_ready(self, data):

        if data == "OK":
            self.ui.txtInformations.append('<b>Navigate - SUCCESS</b>')
        else:
            self.ui.txtInformations.append('<b>Navigate - FAILED</b>')
        
        if self.navigate_thread.isFinished():
            """
            Do Search Part, after navigate success
            """
            self.do_search_part()

    def do_search_part(self):
        """
        Process search part thread
        """
        self.ui.progbarTotal.setMaximum(self.count_code)
        self.search_part_thread = SearchPartThread(self.supplier, self.list_code_to_search)
        self.search_part_thread.sig.connect(self.__on_search_part_ready)
        self.threads.append(self.search_part_thread)
        self.search_part_thread.start()

    def __on_search_part_ready(self, data):

        i = data.get('i')
        self.partnumber = data.get('partnumber')
        i = (int(self.count_success)+int(self.count_failed))+1
        if data.get('status') == "START":
            self.ui.lblWorkingOnValue.setText(self.partnumber)
            self.ui.lblTotal.setText('%d of %d'%(i, self.count_code))
        else:
            self.ui.progbarTotal.setProperty("value",i)
            if data.get('status') == "OK":
                self.result_data += data.get('data')
                self.count_success += 1
                self.ui.lblConfirmedValue.setText('%d/%d'%(self.count_success, self.count_code))
                information_text = '<b>Search %s - SUCCESS</b>' % self.partnumber
            else:
                self.failed_code.append(self.partnumber)
                self.count_failed += 1
                self.ui.lblFailedValue.setText('%d/%d'%(self.count_failed, self.count_code))
                information_text = '<b>Search %s - FAILED</b>. => Did not return any result' % self.partnumber
            self.ui.txtInformations.append(information_text)
        
        if self.search_part_thread.isFinished():
            """
            Do after search part thread finished
            """
            self.supplier.CloseDriver()
            for thread in self.threads:
                thread.terminate()
            self._remove_saved_state()
            self.__new_state()
            self.set_login_false()

    def edit_settings(self):
        """
        Edit setting info dialog
        """
        if self.settings.exec_() == QtGui.QDialog.Accepted:
            self.ui.txtInformations.append('<b>Scrape Settings was updated</b>')

    def select_csv(self):

        """
        To select CSV file
        """

        #self.ui.setupUi(self)
        fname = QtGui.QFileDialog.getOpenFileName(self, 'Open file','','CSV Files (*.csv)')
        if fname:
            f = open(fname, 'r')
            data_df = pandas.read_csv(f)

            select_column = SelectColumn(list(data_df.columns.values))
            if select_column.exec_() == QtGui.QDialog.Accepted:
                self._remove_saved_state()
                self.__new_state()
                self.set_login_false()

                column = str(select_column.column)
                self.list_code = list(data_df[column].values)
                self.list_code = filter(lambda v: v == v, self.list_code)
                self.list_code_to_search = self.list_code

        self.count_code = len(self.list_code)

    def set_supplier_selected(self):
        """
        Set supplier from current saved supplier
        """
        if str(self.ui.radioSupplier1.text()) == self.supplier_name:
            self.ui.radioSupplier1.setChecked(True)
        elif str(self.ui.radioSupplier2.text()) == self.supplier_name:
            self.ui.radioSupplier2.setChecked(True)
        elif str(self.ui.radioSupplier3.text()) == self.supplier_name:
            self.ui.radioSupplier3.setChecked(True)
        elif str(self.ui.radioSupplier4.text()) == self.supplier_name:
            self.ui.radioSupplier4.setChecked(True)
        elif str(self.ui.radioSupplier5.text()) == self.supplier_name:
            self.ui.radioSupplier5.setChecked(True)
        elif str(self.ui.radioSupplier6.text()) == self.supplier_name:
            self.ui.radioSupplier6.setChecked(True)
        elif str(self.ui.radioSupplier7.text()) == self.supplier_name:
            self.ui.radioSupplier7.setChecked(True)

    def get_supplier_selected(self):
        """
        To get selected supplier value
        """
        selected_suplier = ''
        supplier_names = ''
        if self.ui.radioSupplier1.isChecked():
            selected_suplier = Hotbray()
            supplier_names = self.ui.radioSupplier1.text()
        elif self.ui.radioSupplier2.isChecked():
            selected_suplier = DavidManners()
            supplier_names = self.ui.radioSupplier2.text()
        elif self.ui.radioSupplier3.isChecked():
            selected_suplier = FPS()
            supplier_names = self.ui.radioSupplier3.text()
        elif self.ui.radioSupplier4.isChecked():
            selected_suplier = Bearmach()
            supplier_names = self.ui.radioSupplier4.text()
        elif self.ui.radioSupplier5.isChecked():
            selected_suplier = 'Land Rover Unipart Url'
            supplier_names = self.ui.radioSupplier5.text()
        elif self.ui.radioSupplier6.isChecked():
            selected_suplier = 'Jaguar Unipart Url'
            supplier_names = self.ui.radioSupplier6.text()
        elif self.ui.radioSupplier7.isChecked():
            selected_suplier = SNG()
            supplier_names = self.ui.radioSupplier7.text()

        self.supplier = selected_suplier
        self.supplier_name = str(supplier_names)
        return self.supplier

    def save_to(self):
        """
        Set Directory Save 
        """
        selected_dir = QtGui.QFileDialog.getExistingDirectory(self, "Select Directory")
        if selected_dir:
            pathdir = self.path_dir.set_directory(selected_dir)
            self.ui.txtSaveTo.setPlainText(pathdir)
            self._save_to_btn()

    def save_action(self):

        import time
        now = time.strftime("%m-%d-%y_%H-%M")
        """
        Action Save
        """
        if not self.result_data:
            return QtGui.QMessageBox.warning(self, "Alert", "Please start action first.")

        supplier_path = '/'.join((self.path_dir.get_directory(), self.supplier_name))
        if not os.path.exists(supplier_path):
            os.makedirs(supplier_path)
        # today = date.today().strftime('%d-%m-%Y')
        fname = '/'.join((supplier_path,now))

        # Create success.csv
        david_manners_header = ['Search query', 'Chosen part number', 'Supplied part', 'Description', 'Part Type', 'Weight', 'Retail price', 'Your discount price', 'Vat', 'Gross price', 'Usual availability', 'Quantity required', 'date', 'time']
        bearmach_header = ['Part Number', 'Brand', 'Desc', 'Trade', 'Retail', 'Stock', 'Surchg']
        hotbray_header = ['Part', 'Description', 'Stock', 'Comments']
        fps_header = ['Alts', 'Part', 'Description', 'Brand', 'Branch', 'Sheffield', 'Price', 'Unit']
        sng_header = ['Description', 'Part Number', 'Buy Price', 'Availability']

        header ={'David Manners': david_manners_header, 'Bearmach': bearmach_header, 'Hotbray':hotbray_header, 'FPS':fps_header, 'SNG Barratt':sng_header}

        success_fname = '_'.join((fname,'success.csv'))
        with open(success_fname, 'w') as csvfile:
            export = csv.writer(csvfile, delimiter=',', lineterminator='\n',)
            export.writerow(header[self.supplier_name])

            data = []
            if len(self.result_data):
                for res_data in self.result_data:
                    data.append(res_data)

            export.writerows(data)

        # Create failed.csv
        failed_fname = '_'.join((fname,'failed.csv'))
        with open(failed_fname, 'w') as csvfile:
            export = csv.writer(csvfile, delimiter=',', lineterminator='\n')
            data = []
            if len(self.failed_code):
                for res_data in self.failed_code:
                    data.append([res_data])
            export.writerows(data)
        
        self.ui.txtInformations.append('<b>Result data has been saved to  ' + supplier_path + '.</b>')

"""
Thread for navigate
"""
class NavigateThread(QtCore.QThread, QtCore.QObject):

    sig = QtCore.pyqtSignal(object)

    def __init__(self, supplier):
        QtCore.QThread.__init__(self)
        self.supplier = supplier

    def run(self):
        is_success = self.supplier.Navigate()
        if is_success:
            self.sig.emit("OK")
        else:
            self.sig.emit("FAIL")

"""
Thread for Search
"""
class SearchPartThread(QtCore.QThread, QtCore.QObject):

    sig = QtCore.pyqtSignal(object)

    def __init__(self, supplier, partnumber):
        QtCore.QThread.__init__(self)
        self.supplier = supplier
        self.partnumber = partnumber

    def run(self):
        i=0
        for part in self.partnumber:
            i+=1
            result = {'partnumber':part, 'status':'START', 'i':i}
            self.sig.emit(result)
            is_success = self.supplier.DoSearchPart(part)
            if is_success:
                result['status'] = "OK"
                result['data'] = is_success
                self.sig.emit(result)
            else:
                result['status'] = "FAIL"
                self.sig.emit(result)

if __name__ == '__main__':

    app = QtGui.QApplication(sys.argv)
    window = Main()
    window.show()
    sys.exit(app.exec_())