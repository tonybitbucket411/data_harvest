# README #

Myton Automotive - Data Harvester v2

### What is this repository for? ###

This repository is for development data harvester. That is a program to get part availability and price.

### System Requirement ###

* Python 2.7
* `pip` installed. Installation
* `virtualenvwrapper` installed. Installation


### Installation ###

* Create and activate virtual environtment, `mkvirtualenv data_harvest` then `workon data_harvest`
* Install all requirements, `pip install -r requirements.txt`

### Running ###

To run this program, please open the terminal, and run `python main.py`




    