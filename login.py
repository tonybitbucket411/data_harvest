__author__ = 'Ai Jogja'

from PyQt4 import QtCore, QtGui
# Import UI
from harvest_ui.login_dialog_ui import Ui_LoginDialog

"""
Login Dialog
This run after user select supplier, and start button
"""
class Login(QtGui.QDialog):

    def __init__(self,supplier):
        QtGui.QWidget.__init__(self)
        self.supplier = supplier
        self.ui = Ui_LoginDialog()
        self.ui.setupUi(self)
        self.ui.btnCancel.clicked.connect(self.close)
        self.ui.btnLogin.clicked.connect(self.LoginAction)

    def LoginAction(self):
        email = self.ui.txtEmail.text()
        password = self.ui.txtPassword.text()
        if email and password:
            self.__disable_button__()
            self.threads = []
            self.login_thread = LoginThread(self.supplier, str(email), str(password))
            self.login_thread.sig.connect(self.on_login_ready)
            self.threads.append(self.login_thread)
            self.login_thread.start()

        else:
            QtGui.QMessageBox.warning(self, 'Alert', "Email and Password are required!")

    def on_login_ready(self, data):
        if data == "OK":
            self.accept()
            self.login_thread.terminate()
        else:
            self.close()

    def __disable_button__(self):
        self.ui.txtEmail.setDisabled(True)
        self.ui.txtPassword.setDisabled(True)
        self.ui.btnLogin.setDisabled(True)
        self.ui.btnCancel.setDisabled(True)

    def __enable_button__(self):
        self.ui.txtEmail.setDisabled(True)
        self.ui.txtPassword.setDisabled(True)
        self.ui.btnLogin.setDisabled(False)
        self.ui.btnCancel.setDisabled(True)

"""
Thread for login dialog
"""
class LoginThread(QtCore.QThread, QtCore.QObject):
    sig = QtCore.pyqtSignal(object)

    def __init__(self, supplier, email, password):
        QtCore.QThread.__init__(self)
        self.supplier = supplier
        self.email = email
        self.password = password

    def run(self):
        is_login = self.supplier.Login(self.email, self.password)
        if is_login:
            self.sig.emit("OK")
        else:
            self.sig.emit("FAIL")
